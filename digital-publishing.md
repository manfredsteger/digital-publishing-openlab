# Lernziele

- Dokumentspezifische Eigenschaften kennenlernen
- Den Einsatzzweck von Markdown kennen und verstehen lernen
- Dokumente in verschiedene Formate konvertieren
- Ein git-Repositorium als zentralen Speicherort nutzen
- Versionierung verstehen und anwenden erlernen
- Erste Schritte im cross- und multipublishing machen
- Administrative Einrichtung von Maschine-zu-Maschine-Kommunikation (GitLab / WordPress)

# Schritt für Schritt Anleitung  {#schritt-fuer-schritt-anleitung}

Digital Publishing ist mittlerweile nicht nur für Verlage oder Agenturen ein täglicher Bestandteil, auch im wissenschaftlichen und schulischen Umfeld gehören digitale Publikationen sowie Materialien zum Alltag. Umso wichtiger wird es, die Bestände feinsäuberlich zentralisiert zu _lagern_, zu _verwalten_ und _zeiteffizient_ auf einem aktuellen Stand zu halten. Teilautomatisierung spielt bei dieser Toolchain eine tragende Rolle und kann für viele mediale Produkte und sogar für die Kommunikation im Team ein zunehmend positiver Koeffizient sein.

## Zentrale Datei- und Versionsverwaltung

Damit Dokumente von mehreren Personen kollaborativ genutzt werden können, wird ein Dateiverwaltungsdienst benötigt. Je nach Anwendungsfall oder Organisation steht zu Beginn die Entscheidung an, soll der Dienst selbst gehostet oder online bei einem Anbieter gemietet werden. Im Nutzungsszenario von OER bietet sich ein sog. **öffentliches Repositorium** an, das bei den online Anbietern [GitLab.com](https://about.gitlab.com/) und [GitHub.com](https://github.com/) kostenlos und nur mit einer nennenswerten Klausel behaftet ist – alles ist immer öffentlich und darf auch weitergenutzt ([fork](https://de.wikipedia.org/wiki/Fork)) werden.